/********************************************************************************
** Form generated from reading UI file 'testgrid.ui'
**
** Created by: Qt User Interface Compiler version 5.15.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTGRID_H
#define UI_TESTGRID_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TestGrid
{
public:

    void setupUi(QWidget *TestGrid)
    {
        if (TestGrid->objectName().isEmpty())
            TestGrid->setObjectName(QString::fromUtf8("TestGrid"));
        TestGrid->resize(400, 300);

        retranslateUi(TestGrid);

        QMetaObject::connectSlotsByName(TestGrid);
    } // setupUi

    void retranslateUi(QWidget *TestGrid)
    {
        TestGrid->setWindowTitle(QCoreApplication::translate("TestGrid", "Whippet", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TestGrid: public Ui_TestGrid {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTGRID_H
