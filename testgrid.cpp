#include "testgrid.h"
#include "game-data.h"
#include "ui_testgrid.h"

TestGrid::TestGrid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestGrid)
{
    ::game_data::count %= 7;
    ui->setupUi(this);
    pix = std::make_unique<QPixmap>("image-assets/whippet.png");
    if( ::game_data::count == 0 )
      pix = std::make_unique<QPixmap>("image-assets/smallball.jpg");
    labelp = std::make_unique<QLabel>();
    labelp->setPixmap(*pix);
    labelt = std::make_unique<QLabel>();
    labelt->setText(::game_data::players[
      ::game_data::count++
    ]);
    vlay = std::make_unique<QVBoxLayout>(this);
    vlay->addWidget(labelp.get());
    vlay->addWidget(labelt.get());    
}

TestGrid::~TestGrid()
{
    delete ui;
}
