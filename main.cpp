#include "testgrid.h"
#include <QApplication>
#include <QGridLayout>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGridLayout * gridLayout = new QGridLayout;
    TestGrid b0;
    TestGrid u0;
    TestGrid v0;
    TestGrid w0;
    TestGrid u1;
    TestGrid v1;
    TestGrid w1;
    gridLayout->addWidget(&b0, 0, 0, 4, 1);
    gridLayout->addWidget(&u0, 0, 1, 1, 1);    
    gridLayout->addWidget(&v0, 0, 2, 1, 1);    
    gridLayout->addWidget(&w0, 0, 3, 1, 1);
    gridLayout->addWidget(&u1, 2, 1, 1, 1);
    gridLayout->addWidget(&v1, 2, 2, 1, 1);    
    gridLayout->addWidget(&w1, 2, 3, 1, 1);        
    QWidget * win = new QWidget();
    win->setLayout(gridLayout);
    win->setWindowTitle("Robo Whippets");
    win->show();
    return a.exec();
}
