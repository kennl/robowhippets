#ifndef TESTGRID_H
#define TESTGRID_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>

#include <memory>

namespace Ui {
class TestGrid;
}

class TestGrid : public QWidget
{
    Q_OBJECT

public:
    explicit TestGrid(QWidget *parent = 0);
    ~TestGrid();

private:
    Ui::TestGrid* ui;
    std::unique_ptr<QLabel> labelp;
    std::unique_ptr<QPixmap> pix;
    std::unique_ptr<QLabel> labelt;
    std::unique_ptr<QVBoxLayout> vlay;
};

#endif // TESTGRID_H
